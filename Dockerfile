#This is a sample Image
FROM alpine
MAINTAINER psc85@mailbox.org

RUN apk add --no-cache gcc musl-dev curl-dev libcurl make curl \
&& wget https://netcologne.dl.sourceforge.net/project/esniper/esniper/2.35.0/esniper-2-35-0.tgz \
&& tar -xf esniper-2-35-0.tgz \
&& cd esniper-2-35-0 \
&& ./configure \
&& make \
&& make install \
&& cd / \
&& rm esniper-2-35-0.tgz \
&& rm -r esniper-2-35-0 \
&& apk del --no-cache gcc musl-dev curl-dev libcurl make

COPY .esniper /root/

CMD [“echo”,”Image created”]
